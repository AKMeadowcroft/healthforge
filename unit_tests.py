from convert_data import *

def test_findPatients_return_type():
    assert type(findPatients("data\\patients.json"))==list
    
def test_findPatients_total_pats():
    assert len(findPatients("data\\patients.json"))==4
    
def test_findPatients_pats_total_keys():
    pats=findPatients("data\\patients.json")
    for i in pats:
        assert len(i.keys())==5
    
def test_loadLabCodes_return_dict():
    assert type(loadLabCodes("data\\labresults-codes.csv"))==dict

def test_loadLabCodes_total_keys():
    assert len(loadLabCodes("data\\labresults-codes.csv").keys())==133
    
def test_loadLabCodes_value_is_len2_list():
    t=loadLabCodes("data\\labresults-codes.csv")
    for key in t.keys():
        assert type(t[key])==list
        assert len(t[key])==2

def test_stripToValue_is_number():
    assert float(stripToValue('abc_!12.1bbe'))==12.1
    
def test_loadLabResults_return_type():
    lr=loadLabResults("data\\labresults.csv")
    assert type(lr)==dict

def test_loadLabResults_num_pats():
    lr=loadLabResults("data\\labresults.csv")
    assert len(lr.keys())==4
    
    
def test_makePatientLabResults_returns_empty_for_no_lab_results():
    lr=loadLabResults("data\\labresults.csv")
    lc=loadLabCodes("data\\labresults-codes.csv")
    p=makePatientLabResults("111",lr,lc)
    assert p==[]
    
def test_makePatientLabResults_return_type():
    lr=loadLabResults("data\\labresults.csv")
    lc=loadLabCodes("data\\labresults-codes.csv")
    p=findPatients("data\\patients.json")
    o=makePatientLabResults(p[0]['identifiers'][0],lr,lc)
    assert type(o)==list    
    assert type(o[0])==dict
    assert len(o[0].keys())==3
    assert type(o[0]['panel'])==list
    assert type(o[0]['panel'][0])==dict
    assert len(o[0]['panel'][0].keys())==6
    
def test_all_lab_results_made_into_panels():
    lr=loadLabResults("data\\labresults.csv")
    lc=loadLabCodes("data\\labresults-codes.csv")
    p=findPatients("data\\patients.json")
    pan_total=0
    for pat in p:
        pat['dateOfBirth']=pat['dateOfBirth'][:10]
        pat['lab_results']=makePatientLabResults(pat['identifiers'][0],lr,lc)
        for d in pat['lab_results']:
            pan_total=pan_total+len(d["panel"])
        
    lr_lines=0
    for key in lr.keys():
        lr_lines=lr_lines+len(lr[key])
    assert lr_lines==pan_total
        

# def test_createOutput_is_json():
    # createOutput([],file='test.json')
    # assert 
