# Notes for user

To produce output.json, from the terminal, in the directory containing
convert_data.py, run "python convert_data.py" with the folder (called data) 
containing patients.json, labresults.csv, and labresults-codes.csv in the 
same directory. This will generate the output in the current working directory.
