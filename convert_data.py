import os
from datetime import datetime
import json
import string


def findPatients(file,directory=os.getcwd()):  
    '''
    Inputs: json file of patient data
    Outputs: list of patient data profiles (dict format)
    '''
    #file="data\\patients.json"
    js=open(os.path.join(directory,file))
    pdata=json.load(js)
    js.close()
    return pdata

def formatDate(date_in,format):
    '''
    Inputs: date_in as string and format of date_in as string.
    Outputs: reformated date as string
    '''
    date_out=datetime.strptime(date_in,format)
    return date_out.strftime('%Y-%m-%d')

def createOutput(pdata,file='output.json',ind=4):
    '''
    Inputs: pdata, as dictionary
    Outputs: output pdata to json file
    '''
    s=json.dumps({"patients":pdata},indent=ind)
    f=open(file,'w')
    print(s,file=f)
    f.close()

    
def loadLabCodes(filename):
    '''
    Inputs: csv
    Outputs: dictionary of type {key=code:value=[code,description]}
    '''
    f = open( filename, 'r', encoding="utf8" )
    
    #empty dictionary for codes
    codes={}
    for line in f:
        #split line by commas
        l=line.strip().split(',')
        
        #skip first line
        if l[0]=='key':
            continue

        desc=''
        for i in range(2,len(l)):
            desc+=l[i]
        translator=str.maketrans('','','"') #remove " for presentation
        #add code to dictionary
        codes[l[0]]=[l[1],desc.translate(translator)]
    f.close()
    return codes

def stripToValue(result):
    rem=string.ascii_letters+string.punctuation[:13]+string.punctuation[14:]
    translator=str.maketrans('','',rem)
    return result.translate(translator)

def loadLabResults(filename):
    '''
    Inputs: csv - 3 columns
    Outputs: dictionary of type {key=code:value=[code,description]}
    '''
    f = open( filename, 'r', encoding="utf8" )
    #make empty dictionary of patients
    patient_results={}

    for line in f:
        #split line by commas
        l=line.strip().split(',')
        #skip first line
        if l[0]=='HospID':
            continue
        #if patient code in dictionary, append result line to record
        elif l[0] in patient_results.keys():
            patient_results[l[0]].append(l[1:])
        #else create a key for patient code, assign value as list with result 
        else:
            patient_results[l[0]]=[l[1:]]

    f.close()
    return patient_results   
    
    
def makePatientLabResults(patient_key,patient_results,lab_codes):
    '''
    Inputs: ID number of patient (patient_key), 
            dictionary of all patient results (patient_results),
            dictionary of lab codes.
    Outputs: All results for patient with ID=patient_key as a list,
             each element in list is a dict with timestamp and profile for each test,
             with panels for each result.
    '''
    #check patient_key has matching record in lab_results
    if patient_key in patient_results.keys():
        tests=patient_results[patient_key]
    else:
        print("No lab results found for {0}".format(patient_key))
        return []
    
    #set last value for test comparison (need to group by "panel")
    last='start'
    resN=4
    checker={} # dictionary to check test result type not in current panel
    output=[] # to collect output for patient

    for t in tests:
        #extract test code and look up test details from lab_codes
        code=lab_codes[t[-4]]
        #get result, strip to bare value (has extra characters)
        
        #make first entry
        if last=='start':
            #set new last value
            last=t[0]
            #add test code to checker
            checker[code[0]]=1
            #get timestamp
            stamp=formatDate(t[1],'%d/%m/%Y')
            #get test name and code
            profile={"name":t[2],"code":t[3]}
            #generate first panel. Get SNOMED code, description, value, units, lower ref, upper ref
            panel=[{"code":code[0],"label":code[1],"value":stripToValue(t[resN]), "unit":t[-3],"lower":t[-2],"upper":t[-1]}]
            #advance resN
            resN+=1
            
        #reset for each new test
        elif t[0]!=last or code[0] in checker.keys():
            #append current set of lab_results
            output.append({"timestamp":stamp,"profile":profile,"panel":panel})
            #reset resN
            resN=4
            #set new last value
            last=t[0]
            #add test code to checker
            checker={code[0]:1}
            #get timestamp
            stamp=formatDate(t[1],'%d/%m/%Y')
            #get test name and code
            profile={"name":t[2],"code":t[3]}
            #reset panel. Get SNOMED code, description, value, units, lower ref, upper ref
            panel=[{"code":code[0],"label":code[1],"value":stripToValue(t[resN]), "unit":t[-3],"lower":t[-2],"upper":t[-1]}]
            #advance resN
            resN+=1      
        else:
            checker[code[0]]=1 
            #get SNOMED code, description, value, units, lower ref, upper ref. Append to current panel
            panel.append({"code":code[0],"label":code[1],"value":stripToValue(t[resN]), "unit":t[-3],"lower":t[-2],"upper":t[-1]})
            #advance resN
            resN+=1
            
    #append final lab_result
    output.append({"timestamp":stamp,"profile":profile,"panel":panel})

    return output
    
    
    
    
def combineAllData(directory=os.getcwd()):
    '''
    Inputs: directory containing patients.json, labresults.csv,
            and labresults-codes.csv
    Outputs: output.json, profiling all patient data
    '''
    #within directory, make paths for patient.json, labresults-codes.csv,
    #and labresults.csv
    pat_file=os.path.join(directory,"patients.json")
    cod_file=os.path.join(directory,"labresults-codes.csv")
    res_file=os.path.join(directory,"labresults.csv")
    
    #load patient data, lab codes, and lab results
    pat_data=findPatients(pat_file)
    lab_code=loadLabCodes(cod_file)
    lab_res=loadLabResults(res_file)
    
    #loop through patients, creating new field with all lab_res for patient
    for pat in pat_data:
        pat['dateOfBirth']=pat['dateOfBirth'][:10] #remove time part of DoB
        pat['lab_results']=makePatientLabResults(pat['identifiers'][0],lab_res,lab_code)
    
    #output to json file
    createOutput(pat_data)    
    

# how to handle missing patient data?
# how to handle missing lab codes?
    
if __name__=="__main__":
    combineAllData(directory=os.path.join(os.getcwd(),"data"))


    
